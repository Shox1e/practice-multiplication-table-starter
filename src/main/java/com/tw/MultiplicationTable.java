package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (!isValid(start,end))return null;
        return generateTable(start,end);
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start)&&isStartNotBiggerThanEnd(start,end)&&isInRange(end);
    }

    public Boolean isInRange(int number) {
        return number<=1000&&number>=1;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start<=end;
    }

    public String generateTable(int start, int end) {
        StringBuilder result = new StringBuilder();
        for (int i = start; i <= end; i++) {
            result.append(generateLine(start,i));
            if(i!=end){
                result.append("\r\n");
            }
        }
        return String.valueOf(result);
    }

    public String generateLine(int start, int row) {
        StringBuilder result = new StringBuilder();
        for (int i = start; i <=row; i++) {
            result.append(generateSingleExpression(i, row));
            if (i!=row){
                result.append("  ");
            }
        }
        return String.valueOf(result);
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return multiplicand+"*"+multiplier+"="+multiplicand*multiplier;
    }
}
